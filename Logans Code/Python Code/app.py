import random
import tkinter as tk
from tkinter import ttk
import time
import serial

       
  
ser = serial.Serial('COM3', 9600, timeout=1)
time.sleep(3)
ser.write(100)

def check_state():
    user_number = -1
   
    user_input = ser.readline()
    
    
    if user_input[0] ==102:
        print("Trough is full...")
        time.sleep(0.1) 
        user_number = 1

        
    elif user_input[0] ==104:
        print("Trough is half full")
        time.sleep(0.1)
        user_number = 2
  
    elif user_input[0] ==101:
        print("Trough is empty")
        time.sleep(0.1)
        user_number = 3
     
    elif user_input[0] == 111:
        print("Trough is overflowing")
        time.sleep(0.1)
        user_number = 0
        
    else:
        check_state()
        
    return(user_number)
           
   
   


class app():
    
    def __init__(self):
        self.root = tk.Tk()
        #self.label = tk.Label(self, text = states[value], font = ("Arial Bold", 50))
        self.root.minsize(600, 400)

        self.states = ["Overflowing", "Full", "Half Full", "Empty"]
        
        
        
        #self.canvas = tk.Canvas(self, width = 200, height = 200)
   
        
        
        self.tab_parent = ttk.Notebook(self.root)
        
        tab1 = ttk.Frame(self.tab_parent)
        tab2 = ttk.Frame(self.tab_parent)
        
        self.tab_parent.add(tab1, text="Current Status")
        self.tab_parent.add(tab2, text="Graphs")
        
        self.tab_parent.pack(expand=1, fill="both")       
        
        
        
       
        self.label = tk.Label(tab1,text="", font=("Arial Bold", 50))
        self.time = tk.Label(tab1,text="", font=("Arial Bold", 50))
        self.status = tk.Label(tab1,text="", fg="seashell4", font=("Arial Bold", 20))
        self.canvas = tk.Canvas(tab1,width = 200, height = 200)
        self.count = 0
        
        #self.label.grid(row =0, column =0, padx=15,pady=15)
        #self.time.grid(row = 1, column =0, padx=15,pady=15)
        
        self.time.pack()
        self.label.pack()
        self.status.pack()
        self.canvas.pack()
        self.update()
        
        tab_parent = ttk.Notebook(self.root)
        


        self.root.mainloop()
        
       

    def update(self):
       
        time.sleep(2) # wait for the serial connection to initialize
        
              
        self.user_input = check_state()
         
    
        self.label.configure(text = "Status: " + self.states[self.user_input])
        
        self.img = self.select_image()
        #img = tk.PhotoImage(file=r'Empty.png')
        
        self.time_label() 
        self.status_display()        
        
        img = self.select_image()
        
        self.img = img
        
        self.canvas.create_image(0,0,  image=img, anchor = "nw")                                
                                 
        #self.root.after(180000, self.update())
        
    def time_label(self):
        now = time.strftime("%H:%M")
        self.time.configure(text=now )
        self.root.after(1000, self.time_label)
        
        #self.count = self.count + 1
        
        #if self.count == 2:
            #self.update()
            #self.count = 0
        #self.root.after(180000, self.status_display())
        
        
    def status_display(self):
        
        
        now = time.strftime("%H:%M")
        
        self.status.configure(text="Last Updated " + now)
        
                        
    def select_image(self):
        
        if (self.user_input == 3):
            img = tk.PhotoImage(file=r'Empty.png')
            
        if (self.user_input == 2):
            img = tk.PhotoImage(file=r'half full.png') 
            
        if (self.user_input == 1):
                img = tk.PhotoImage(file=r'full.png')
                
        if (self.user_input == 0):
                img = tk.PhotoImage(file=r'overflow.png')           
        
        return(img)

        
        
app=app()   




    #print(states[value])
    
    


#button = tkinter.Button(top, text = "Update", command = update())
#button.grid(column = 0, row = 2)

   




    
